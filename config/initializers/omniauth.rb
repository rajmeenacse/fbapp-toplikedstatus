module Facebook
  CONFIG = YAML.load_file("#{Rails.root}/config/myapp/facebook.yml")['development']
  APP_ID = CONFIG['app_key']
  SECRET = CONFIG['secret_key']
  CALLBACK_URL = CONFIG['callback_url']
end

Rails.application.config.middleware.use OmniAuth::Builder do

  provider :facebook, Facebook::APP_ID.to_s, Facebook::SECRET.to_s, 
           :scope => 'email, read_stream,offline_access,publish_stream',
           :iframe => true,
           :setup => true

end

OmniAuth.config.full_host = 'http://apps.facebook.com/topmostlikedstatus/'

OmniAuth.config.on_failure = Proc.new do |env|

  SessionsController.action(:destroy).call(env)

end
