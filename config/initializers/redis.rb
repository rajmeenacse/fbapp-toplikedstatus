rails_root = ENV['RAILS_ROOT'] || File.dirname(__FILE__) + '/../..'
rails_env = ENV['RAILS_ENV'] || 'development'

$resque_config = YAML.load_file(rails_root + '/config/myapp/default.yml')

uri = URI.parse($resque_config[rails_env])
REDIS_SERVER = Redis.new(host: uri.host, port: uri.port, password: uri.password)

