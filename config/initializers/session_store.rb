# Be sure to restart your server when you modify this file.

FbappToplikedstatus::Application.config.session_store :cookie_store, :key => '_toplikedstatus_session'

=begin
FbappToplikedstatus::Application.config.session_store :redis_store, :key => '_toplikedstatus_session'


# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# FbappToplikedstatus::Application.config.session_store :active_record_store

FbappToplikedstatus::Application.config.session_options = {
  :cookie_only => false,
  :httponly => true,
  :key => '_likedphotos_session',
  :expire_after => 60.minutes,
  :path => "/",
  :server => REDIS_SERVER,
  :namespaces => {session: "myapps_", cache: "myappc_"},
  :secure => false
}

=end
