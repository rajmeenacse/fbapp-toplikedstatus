# -*- encoding : utf-8 -*-
FbappToplikedstatus::Application.routes.draw do

  match '/auth/:provider/callback' => 'sessions#create'
  match '/auth/:provider' => 'sessions#setup'
  match '/auth/failure' => 'sessions#failure'
  match '/logout' => 'sessions#destroy', :as => :logout

  match  '/facebookapp' => 'facebook_app#create', :as => :app

  root :to => 'home#index'

end
