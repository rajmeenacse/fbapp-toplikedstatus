class AlertMailer < ActionMailer::Base
  default sender: "topmostlikedstatus@gmail.com",
          from: "topmostlikedstatus@gmail.com",
          to: "rajmeena.cse@gmail.com"

  def error(e, params, options = {})
    @exception = e
    @fullpath = options[:fullpath]
    @params = params

    if e.respond_to?(:response) and e.response and e.response.respond_to?(:read_body)
      @error_body = "#{e.inspect}: #{e.response.read_body.gsub("\n","")}"
    else
      @error_body = e
    end

    mail(subject: "[Error Myapp] #{e.message}") do |format|
      #format.text { render text: e.backtrace.join("\n") }
      format.text
    end.deliver
  end
end

