class FacebookStatus < ActiveRecord::Base

  class << self

    def updated_statuses(graph_details)
      @graph_details = graph_details
      begin
        fb_user = @graph_details.get_object("me")
        #statuses = @graph_details.fql_query("SELECT post_id,likes.count,created_time, message FROM stream WHERE type = '46' AND source_id = #{fb_user["id"]}  LIMIT 25 OFFSET 10")
        statuses = @graph_details.get_connection(fb_user["id"], "statuses", :since =>'2011-12-31T23:59:59+0000',:until =>'2012-12-31T23:59:59+0000' )

        status_array = Array.new
        status_array.push(statuses)

        while !statuses.next_page_params.nil?
          statuses = statuses.next_page
          status_array.push(statuses)
        end

        formated_status = Hash.new

        for status in status_array[0]
          formated_status[status["message"]] = counted_likes(status["id"]),status["updated_time"]
        end

        result = formated_status.sort {|a1,a2| a2[1]<=>a1[1]}

        if result.length > 0
          result =result.first()
          toplikedstatus = "My Most Liked Status of 2012 #{"\n"} Status: #{result[0]}, No of Likes #{result[1][0]} #{"\n"} updated on #{formated_time(result[1][1])}"
          wall_post(toplikedstatus)
          return toplikedstatus
        end

      rescue => e
          raise e
          Rails.logger.info e
      end
    end

    def counted_likes(object_id)
      user_likes = @graph_details.fql_query("SELECT user_id from like WHERE object_id = #{object_id}")
      return user_likes.length
    end

    def formated_time(time)
      Time.parse(time).strftime("%y/%m/%d")
    end

    def wall_post(toplikedstatus)
      @graph_details.put_wall_post(toplikedstatus.to_s)
    end

  end
end
