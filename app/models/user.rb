class User < ActiveRecord::Base

  scope :authenticated, where("access_token IS NOT NULL AND access_token != ''")

  class << self

    def authenticate(auth)
      begin
        create! do |user|
          user.provider = auth["provider"]  
          user.uid = auth["uid"]  
          user.access_token = auth["credentials"]["token"]
          user.email = auth["info"]["email"]
          user.name = auth["info"]["name"]
          user.save! 
        end
      rescue Exception => e
        raise e, "cannot create user record"  
      end
    end

  end
end

