class SessionsController < ApplicationController

  def create  
    auth = request.env["omniauth.auth"]
    user = User.authenticate(auth)
    cookies[:access_token] = user.access_token if user
    redirect_to session.delete(:return_to) || root_url
  end

  def setup
    session[:return_to] = request.referer if params[:redirect_back]
    render :text => "Setup complete.", :status => 404
  end

  def destroy
    cookies[:access_token] = nil
    redirect_to root_url
  end

  def failure
    redirect_to root_url
  end
 
end
