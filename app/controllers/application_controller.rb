class ApplicationController < ActionController::Base

  #protect_from_forgery

  helper_method :current_user, :logged_in?
  around_filter :access_log


  private

  def current_user
    @current_user ||= User.authenticated.find_by_access_token!(cookies[:access_token])
  rescue ActiveRecord::RecordNotFound
    cookies[:access_token] = nil
  end

  def logged_in?
    !!current_user
  end

  def require_login
    unless logged_in?
      respond_to do |format|
        format.html do
          session[:return_to] = request.path
          redirect_to '/auth/facebook?redirect_back=yes'
        end
        format.json do
          head :unauthorized
        end
      end
    end
  end

  def access_log
    request_time = Time.now

    e_status  = nil
    e_message = nil
    e_body    = nil

    begin
      yield
    rescue
      e = $!

      e_message = e_body = "#{e.inspect}"

      if e.respond_to?(:response) and e.response
        e_status = e.response.code if e.response.respond_to?(:code)
        if e.response.respond_to?(:read_body)
          e_body += "#{e.response.read_body.gsub("\n","")}"
          logger.debug e.response
        end
      else
        e_status = "500"
      end

      begin
        unless e_status.to_i == 404
          AlertMailer.error(e, { fullpath: request.original_fullpath })
        end
      rescue
        logger.error("Failed to send alert mail: #{$!}")
      end

      raise

    ensure
      request_method   = request.request_method      || '-'
      request_protocol = request.server_protocol     || '-'
      response_time    = ((Time.now - request_time)*1000).round
      response_status  = e_status || response.status || '-'
      location         = response.location           || '-'
      remote_addr      = request.remote_ip           || '-'
      path             = request.original_fullpath   || '-'
      referer          = request.referer             || '-'
      user_agent       = request.user_agent          || '-'

      logger.info %([access] #{remote_addr} #{response_time}ms "#{request_method} #{path} #{request_protocol}" #{response_status} #{location} "#{referer}" "#{user_agent}")

    end
  end

#error handling

rescue_from Koala::Facebook::APIError, :with => :handle_fb_exception

private
  def handle_fb_exception exception
    if exception.fb_error_type.eql? 'OAuthException'
        redirect_to root_url
    else
      logger.debug "bug in Koala gem; reraising the exception..."
      raise exception
    end
  end
end
