class FacebookAppController < ApplicationController
  before_filter :require_login, :only => [:create]
  before_filter :graph_details

  layout 'facebook_app'

  def create
    begin
      @toplikedstatus = FacebookStatus.updated_statuses(graph_details)
      Rails.logger.info @toplikedstatus
    rescue => e
        raise e
    end
  end

private
  def graph_details
    if current_user
      @graph_details ||= Koala::Facebook::API.new(cookies["access_token"])
    end
  end
end
